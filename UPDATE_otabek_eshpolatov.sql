
do
$$
    BEGIN
        -- Alter the rental duration and rental rates of the film you inserted before to three weeks and 9.99, respectively.
        UPDATE public.film
        SET rental_duration = 21,
            rental_rate     = 9.99
        WHERE title = 'Avengers: Infinity War'
          AND release_year = 2018
          AND original_language_id = 1
          AND rental_duration = 14
          AND rental_rate = 4.99;

        -- Alter any existing customer in the database with at least 10 rental and 10 payment records.
-- Change their personal data to yours (first name, last name, address, etc.). You can use any existing address from the "address" table.
-- Please do not perform any updates on the "address" table, as this can impact multiple records with the same address.

-- Change the customer's create_date value to current_date
        UPDATE public.customer
        SET first_name  = 'Otabek',
            last_name   = 'Eshpulatov',
            email       = 'otabek.eshpulatov@gmail.com',
            address_id  = (SELECT address_id FROM public.address ORDER BY random() LIMIT 1),
            create_date = current_date,
            last_update = now()
        WHERE customer_id IN (SELECT c.customer_id
                              FROM public.customer c
                                       JOIN public.rental r ON c.customer_id = r.customer_id
                                       JOIN public.payment p ON c.customer_id = p.customer_id
                              GROUP BY c.customer_id
                              HAVING COUNT(DISTINCT r.rental_id) >= 10
                                 AND COUNT(DISTINCT p.payment_id) >= 10);

    end;
$$;
